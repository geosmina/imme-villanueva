<?php

namespace Grupo_Villanueva;

use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{
    public function estado(){
        return $this->belongsTo('App\Estado');
    }

    public function municipios(){
        return $this->hasMany(('App\Municipio'));
    }
}
