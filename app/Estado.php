<?php

namespace Grupo_Villanueva;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    public function municipios(){
        return $this->hasManyThrough('App\Municipio', 'App\Estado');
    }

    public function ciudad(){
        return $this->hasMany('App\Estado');
    }
}
