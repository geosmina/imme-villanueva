<?php

namespace Grupo_Villanueva\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class SearchController extends Controller
{
    public function buscarCp(Request $request)
    {

        // Usando la clase Request, toma el input 'term' y lo guarda en una variable.
        $term = $request->input('term');
        // Aquí se guardará en formato JSON el array con los resultados.
        $results = [];
        // Usando el Eloquent de Laravel, tomamos 10 datos de la tabla->columna 'posta_code' que sean como el 'term' buscado.
        $queries = DB::connection('db_postalcodes')
            ->table('postal_code')
            ->where('postal_code', 'LIKE', '%' . $term . '%')
            ->take(10)
            ->get();

        // Hacemos un foreach para tomar analizar los valores obtenidos y sacar datos necesarios guardándolos en este array asociativo.
        foreach ($queries as $query) {
            $results[] = [
                'postal_code' => $query->postal_code,
                'value' => $query->postal_code.', '.$query->neighborhood_display, //Concatenamos los valores que queramos de la base de datos.
            ];
        }
        // Regresamos el array $results como un objeto JSON.
        return Response::json($results);
    }
}
