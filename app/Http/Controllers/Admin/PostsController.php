<?php

namespace Grupo_Villanueva\Http\Controllers\Admin;

use Grupo_Villanueva\Http\Controllers\Controller;
use Grupo_Villanueva\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{
    public function index()
    {

        // Primero se conecta a la base de datos de gvcomm_properties con el alias que le puse (db_properties) por medio de la función ::connection()
        $properties = DB::connection('db_properties')
            ->table('property')
            ->get();

        // Saca todos los posts de la bd
        // $posts = Post::all();

        return view('admin.posts.index', compact('properties'));
    }

    public function create()
    {
        // Toma los estados de la base de datos.

        $estados = DB::connection('db_postalcodes')
            ->table('state')
            ->pluck('state', 'state_id');

        return view('admin.posts.create', compact('estados'));
    }

    public function getCiudad($id)
    {
        $ciudades = DB::connection('db_postalcodes')
            ->table('city')
            ->where('state_id', $id )
            ->pluck('city', 'city_id');

        return json_encode($ciudades);
    }

    // Guarda el post en la base de datos.
    public function store(Request $request)
    {
        // return $request->all();
        $post = new Post;

        $post->title = $request->get('title');
        $post->body = $request->get('body');
        $post->excerpt = $request->get('excerpt');
        // $post->title = $request->get('title');
        // Guarda en la base de datos
        $post->save();

        return back()->with('flash', 'La propiedad se ha almacenado correctamente.');
    }

}
