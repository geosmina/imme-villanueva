<?php

namespace Grupo_Villanueva\Http\Controllers;

use Grupo_Villanueva\Post;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    // Todos los views están dentro de la carpeta frontend. Nada de aquí tiene que ver con lo admin.
    public function inicio(){
        return view('frontend.inicio');
    }

    public function nosotros(){
        return view('frontend.nosotros');
    }

    public function vivirqro(){
        return view('frontend.vivirqro');
    }

    public function contacto(){
        return view('frontend.contacto');
    }
}
