<?php

namespace Grupo_Villanueva;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    // Assuming each Municipality can belong to only one State
    public function state()
    {
        return $this->belongsTo('App\State');
    }

    // Should you ever need this Municipality's Country
    public function country()
    {
        return $this->state->country;
    }
}
