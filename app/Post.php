<?php

namespace Grupo_Villanueva;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    // Deshabilitar protección de asignación masiva (?)
    protected $guarded = [];
}
