import './bootstrap';

$(document).ready(function () {
    // Este .js se compila con Laravel.mix (Webpack). Ejecutar 'npm run dev'.

    console.log('app.js está corriendo...');

    // Variables tomadas del DOM---------------
    // Inputs radio
    let inputsEstado = $('#inputs-estado');
    let inputsPostal = $('#inputs-postal');
    // ----------------------------------------

    // Esconde los inputs de dirección
    inputsEstado.hide();
    inputsPostal.hide();

    // Aquí ve qué radio está seleccionado dentro del namegroup.
    $("input[name=direccion-radio]:radio").click(function () {
        if ($(this).attr("value") == "estado") {
            inputsEstado.show();
            inputsPostal.hide();
            console.log('por estado seleccionado');
        }
        if ($(this).attr("value") == "postal") {
            inputsEstado.hide();
            inputsPostal.show();
            console.log('por postal seleccionado');
        }
    });

    // $('.select-cp').select2({
    //     placeholder: 'Selecciona una opción'
    //     });

    // Supuesta hace los selects dependientes.
    $('select[name="estado"]').on('change', function () {
        let estadoId = $(this).val();

        if (estadoId) {
            $.ajax({
                url: "create/ciudades/get/" + estadoId,
                type: "GET",
                dataType: "json",
                beforeSend: function(){
                    $('select[name="ciudad"').empty();
                },
                success: function (data) {
                    $.each(data, function (key, value) {
                        $('select[name="ciudad"').append('<option value="' + key + '"> ' + value + '</option>');
                    });
                }
            });
        }
    });
});
