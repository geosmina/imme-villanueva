<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    {{-- Layout oficial de todo el frontend --}}
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
    {{-- El str_replace cambia el _ por espacio para imprimir humanamente el título de la aplicación. --}}
    <title>@yield('paginaTitulo') - {{str_replace('_', ' ', config('app.name', 'Grupo Villanueva'))}} </title>
</head>
<body>
    {{-- Navbar --}}
    @include('frontend.includes.navbar')
    {{-- Contenido de otro view se pega aquí. No incluye containers ni nada; éso se define allá. ---------------}}
    @yield('content')
    {{-- ---------------------------------------------------------------------------------------------------- --}}
    {{-- Footer --}}
    @include('frontend.includes.footer')
    <script src="{{asset('js/app.js')}}"></script>
</body>
</html>
