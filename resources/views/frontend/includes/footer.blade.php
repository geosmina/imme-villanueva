<!-- Footer -->
<footer id="frontend-footer" class="fondo-azul text-white page-footer font-small blue pt-4 px-5">
    <!-- Footer Links -->
    <div class="container-fluid text-center text-md-left">
        <!-- Grid row -->
        <div class="row">
            <!-- Grid column -->
            <div class="col-md-6 mt-md-0 mt-3">
                <!-- Content -->
                <h5 class="text-uppercase">Contacto</h5>
                <p><span><b>Grupo Villanueva, S.A.</b></span></p>
                <p>Circuito del Mesón # 126, Fraccionamiento del Prado, Querétaro, Qro. C.P. 76030</p>
            </div>
            <!-- Grid column -->
            <hr class="clearfix w-100 d-md-none pb-3">
            <!-- Grid column -->
            <div class="col-md-3 mb-md-0 mb-3">
                <!-- Links -->
                <h5 class="text-uppercase">Síguenos</h5>
                <ul class="list-unstyled">
                    <li class="pb-2">
                        <a href="https://www.facebook.com/grupovillanuevaqro/" target="_blank"> <img class="img-fluid"
                                src="{{asset('img/svg/fb-logo.svg')}}" alt="{{str_replace('_', ' ', config('app.name', 'Grupo Villanueva'))}} Facebook">
                        </a>
                    </li>
                    <li class="pt-2">
                        <a href="https://twitter.com/VillanuevaGrupo" target="_blank"> <img class="img-fluid" src="{{asset('img/svg/tw-logo.svg')}}"
                                alt="{{str_replace('_', ' ', config('app.name', 'Grupo Villanueva'))}} Twitter"> </a>
                    </li>
                </ul>
            </div>
            <!-- Grid column -->
            <!-- Grid column -->
            <div class="col-md-3 mb-md-0 mb-3">
                <!-- Links -->
                <h5 class="text-uppercase">Links</h5>
                <ul class="list-unstyled">
                    <li>
                        <a href="{{route('inicio')}}">Inicio</a>
                    </li>
                    <li>
                        <a href="{{route('nosotros')}}">Nosotros</a>
                    </li>
                    <li>
                        <a href="{{route('vivirqro')}}">Vivir en Querétaro</a>
                    </li>
                    <li>
                        <a href="{{route('contacto')}}">Contacto</a>
                    </li>
                </ul>
            </div>
            <!-- Grid column -->
        </div>
        <!-- Grid row -->
    </div>
    <!-- Footer Links -->
    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">Todos los derechos reservados ©2019 - <a href="{{ route('inicio') }}">
            Grupo Villanueva, S.A.</a>
    </div>
    <!-- Copyright -->
</footer>
<!-- Footer -->
