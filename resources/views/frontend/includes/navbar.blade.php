<nav class="navbar navbar-expand-lg navbar-light bg-light py-3 px-5">
    <div class="pr-5">
        <a href=" {{ route('inicio') }} "><img src="{{ asset('img/gv-logo.jpg') }}" alt="Grupo Villanueva"></a>
    </div>
    {{-- Botón de menú colapsable --}}
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href=" {{route('inicio')}} ">Inicio <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('nosotros')}}">Nosotros</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('vivirqro')}}">Vivir en Querétaro</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('contacto')}}">Contacto</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">Login</a>
            </li>
        </ul>
    </div>
</nav>
