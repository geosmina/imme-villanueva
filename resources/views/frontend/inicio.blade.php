{{-- Inicio/Index --}}
@extends('layouts.frontend')
@section('paginaTitulo', 'Inicio')


@section('content')
{{-- Cuando se cierra la sesión y redirecciona, muestra este mensaje. --}}
@if(Session::has('flash'))
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{Session::get('flash')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
</div>
@endif


<div id="inicio-slider" class="container-fluid h-100 py-5">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-12 text-center">
            <p class="h1 texto-sombra text-uppercase text-white">Estamos moviendo la tierra</p>
            <p class="h2 texto-sombra  text-white">En compra, venta y gestión de propiedades.</p>
            <p class="h2 texto-sombra  text-white">Somos su referente en <b>Querétaro</b> </p>
        </div>
    </div>
</div>

{{-- Texto 1 --}}
<div class="container-fluid h-100 fondo-azul">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-12 p-5">
            <p class="h1 texto-sombra text-uppercase text-white">Tratándose de Querétaro <b>nosotros somos los expertos</b>
            </p>
            <p class="h4 texto-sombra pt-4 text-white">Sean industriales, comerciales y de servicios o habitacionales,
                <b>Grupo
                    Villanueva</b>
                tiene una extensa selección de propiedades en venta o renta para satisfacer a cada cliente con atención
                personalizada.</p>
        </div>
    </div>
</div>

{{-- Propiedades destacadas --}}
<div class="container-fluid h-100">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-12 p-5">
            <p class="h2 texto-azul text-uppercase ">Propiedades destacadas</p>
        </div>
    </div>
</div>

{{-- Contacto --}}
<div class="container-fluid h-100 fondo-azul">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-sm-6 p-5">
            <p class="h2 text-white text-uppercase texto-sombra">¿Deseas vender o rentar?</p>
            <p class="h3 text-white pt-4 texto-sombra">Contáctenos; lo asesoraremos e incluímos su propiedad en nuestro
                catálogo.</p>
        </div>
        <div class="col-sm-6 p-5">
            <p class="h2 texto-sombra text-white text-uppercase ">(52 442) 228 0183</p>
            <div class="pt-4">
                <button type="button" class="btn btn-primary btn-lg">Contacto</button>
            </div>
        </div>
    </div>
</div>

{{-- Atención personalizada --}}
<div class="container-fluid h-100">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-12 p-5">
            <p class="h3 pb-3"> <span class="texto-azul">Atención personalizada</span> en cada una de las etapas de las
                Bienes Raíces </p>
            <p class="h4 pt-2"> <b>Grupo Villanueva</b> provee servicios completos para desarrolladores y clientes que
                busquen vender, rentar, construir, invertir o vivir en Querétaro. </p>
        </div>
    </div>
</div>

@endsection
