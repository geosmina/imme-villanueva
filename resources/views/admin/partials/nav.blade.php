<!-- Sidebar Menu -->
<ul class="sidebar-menu" data-widget="tree">
    <li class="header">Navegación</li>
    <!-- Optionally, you can add icons to the links -->
<li class="active"><a href="{{ route('admin') }}"><i class="fa fa-home"></i> <span>Inicio</span></a></li>
    <li class="treeview">
        <a href="#"><i class="fa fa-book"></i> <span>Inventario</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
        <li><a href="{{ route('admin.posts.index') }}"><i class="fa fa-eye"></i> Ver propiedades</a></li>
        <li><a href="{{ route('admin.posts.create') }}"><i class="fa fa-pencil"></i> Crear propiedad</a></li>
        </ul>
    </li>
</ul>
<!-- /.sidebar-menu -->
