@extends('admin.adminlayout')

@section('header')
<h1>
    Alta de inmuebles
</h1>

<ol class="breadcrumb">
    <li><a href="{{ route('admin') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Alta de inmuebles</li>
</ol>

@stop

@section('content')

<div class="row">
    <div class="col-md-8">
        <div class="box box-primary">
            <form id="formulario_ingreso" method="post" action="{{ route('admin.posts.store') }}">

                {{-- Se pone por seguridad este token --}}
                {{ csrf_field() }}
                <div class="box-body">
                    {{-- Tipo de operación --}}
                    <div class="form-group">
                        <label>Tipo de operación:</label>
                        <br>
                        <fieldset>
                            <div class="radio-inline"><input type="radio" name="tipo-operacion">Venta</div>
                            <div class="radio-inline"><input type="radio" name="tipo-operacion">Renta</div>
                            <div class="radio-inline"><input type="radio" name="tipo-operacion">Venta/Renta</div>
                        </fieldset>
                    </div>
                    <hr>
                    {{-- Dirección --}}
                    <div class="form-group">
                        <label>Dirección</label>
                        <br>
                        <fieldset>
                            <div id="radio-estado" class="radio-inline"><input type="radio" name="direccion-radio"
                                    value="estado">Por
                                estado</div>
                            <div id="radio-postal" class="radio-inline"><input type="radio" name="direccion-radio"
                                    value="postal">Por
                                código postal</div>
                        </fieldset>
                        <br>
                        {{-- Muestra los estados sacados de la base de datos. --}}
                        <div id="inputs-estado">
                            <div class="form-group">
                                {{-- Estados --}}
                                <label for="">Estado</label>
                                <select name="estado" id="forma-estado" class="form-control">
                                    @foreach ($estados as $estado => $value)
                                    <option value="{{ $estado }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                                <br>
                                {{-- Ciudad --}}
                                <label for="">Ciudad</label>
                                <select name="ciudad" id="form-ciudad" class="form-control">

                                </select>
                                <br>
                                {{-- Colonia --}}
                                <label for="">Municipo</label>
                                <select name="direccion" id="form-municipio" class="form-control dynamic" data-dependant="municipality">
                                    {{-- <option value="">Selecciona el municipio</option> --}}
                                </select>
                            </div>
                        </div>
                        <div id="inputs-postal">
                            <input class="postalCode form-control" type="text" name="postalCode">
                        </div>
                    </div>
                    <hr>
                    {{-- Tipo de promoción --}}
                    <div class="form-group">
                        <label>Tipo de promoción</label>
                        <br>
                        <fieldset>
                            <div class="radio-inline"><input type="radio" name="tipo-promocion">Exclusiva</div>
                            <div class="radio-inline"><input type="radio" name="tipo-promocion">No exclusiva</div>
                        </fieldset>
                    </div>
                    <hr>
                    {{-- Precio de venta --}}
                    <div class="form-group">
                        <label>Precio de venta</label>
                        <div>$ <input type="number" min="1" step="any" /></div>
                    </div>
                    <hr>
                    {{-- Descripción --}}
                    <div class="form-group">
                        <label>Título</label>
                        <br>
                        <input type="text" name="titulo"><br>
                    </div>
                    <hr>
                    {{-- Descripción --}}
                    <div class="form-group">
                        <label>Descripción</label>
                        <textarea id="cuerpoPost" rows="10" name="body" class="form-control" placeholder="Contenido"></textarea>
                    </div>
                    <hr>
                    {{-- Botón submit --}}
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block">Guardar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@stop
