@extends('admin.adminlayout')

@section('header')
<h1>
    Todas las publicaciones
    {{-- <small>Inventario</small> --}}
</h1>

<ol class="breadcrumb">
    <li><a href="{{ route('admin') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Posts</li>
</ol>

@stop

@section('content')

{{-- Contenido dentro del dashboard en AdminLTE --}}

<p class="lead">Usuario autenticado: {{ auth()->user()->name }}</p>

<div class="box">
    <div class="box-header">
        <h3 class="box-title">Propiedades en la base de datos</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="posts-table" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Código de propiedad</th>
                    <th>Título</th>
                    <th>Descripción</th>
                    <th>Calle</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($properties as $property)
                    <tr>
                        {{-- La variable $property la toma del controlador --}}
                        <td>{{ $property->property_code }} </td>
                        <td>{{ $property->title }} </td>
                        <td>{{ $property->description }} </td>
                        <td>{{ $property->street }} </td>
                        {{-- <td>
                            <a href="#" class="btn btn-xs btn-info"> <i class="fa fa-pencil"></i> </a>
                            <a href="#" class="btn btn-xs btn-danger"> <i class="fa fa-times"></i> </a>
                        </td> --}}
                    </tr>

                @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>

@stop
