<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

//Rutas del frontend -------------------------------------------------------
Route::get('/', 'PagesController@inicio')->name('inicio');
Route::get('/nosotros', 'PagesController@nosotros')->name('nosotros');
Route::get('/vivirqueretaro', 'PagesController@vivirqro')->name('vivirqro');
Route::get('/contacto', 'PagesController@contacto')->name('contacto');
//--------------------------------------------------------------------------

//ADMIN: Pone el prefijo 'admin' como grupo. --------------------------------------
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth'], function () {
    Route::get('/', 'AdminController@index')->name('admin'); //Inicio de la página de Administrador.
    Route::get('posts', 'PostsController@index')->name('admin.posts.index'); //Muestra una tabla con todas las propiedades guardadas en la db.
    Route::get('posts/create', 'PostsController@create')->name('admin.posts.create'); //Muestra un formulario para guardar una nueva propiedad.
    Route::get('posts/create/ciudades/get/{id}', 'PostsController@getCiudad');
    Route::post('posts', 'PostsController@store')->name('admin.posts.store'); //Guarda la propiedad en la db.
});
//---------------------------------------------------------------------------------

// Aquí se guarda la función que hace, vía ajax, búsqueda de código postal. Su funcionamiento está descrito en el controlador.
Route::get('/buscarCp', 'SearchController@buscarCp')->name('busqueda-cp');
//-------------------------------------------------------------------------

// Authentication Routes... -------------------------------------------------------------------
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

//  // Registration Routes...
//  if ($options['register'] ?? true) {
//      Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//      Route::post('register', 'Auth\RegisterController@register');
//  }

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

// Email Verification Routes...
if ($options['verify'] ?? false) {
    $this->emailVerification();
}
// ------------------------------------------------------------------------------------------------
